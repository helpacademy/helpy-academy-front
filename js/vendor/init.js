
jQuery(document).ready(function($) {

    /*---------------------------------------------------- */
    /* Preloader
     ------------------------------------------------------ */
    $(window).load(function() {

        // will first fade out the loading animation
        $("#status").fadeOut("slow");

        // will fade out the whole DIV that covers the website.
        $("#preloader").delay(500).fadeOut("slow").remove();

    })

    /*----------------------------------------------------*/
    /*	Back To Top Button
     /*----------------------------------------------------*/
    var pxShow = 300; //height on which the button will show
    var fadeInTime = 400; //how slow/fast you want the button to show
    var fadeOutTime = 400; //how slow/fast you want the button to hide
    var scrollSpeed = 300; //how slow/fast you want the button to scroll to top. can be a value, 'slow', 'normal' or 'fast'

    // Show or hide the sticky footer button
    jQuery(window).scroll(function() {

        if (jQuery(window).scrollTop() >= pxShow) {
            jQuery("#go-top").fadeIn(fadeInTime);
        } else {
            jQuery("#go-top").fadeOut(fadeOutTime);
        }

    });


    /*----------------------------------------------------*/
    /*  Placeholder Plugin Settings
     ------------------------------------------------------ */
    $('input, textarea').placeholder()

    /*----------------------------------------------------*/
    /* Final Countdown Settings
     ------------------------------------------------------ */
    var finalDate = '2016/10/01';

    $('div#counter').countdown(finalDate)
        .on('update.countdown', function(event) {

            $(this).html(event.strftime('<span>%D <em>J</em></span>' +
                '<span>%H <em>H</em></span>' +
                '<span>%M <em>M</em></span>' +
                '<span>%S <em>S</em></span>'));
        });


});