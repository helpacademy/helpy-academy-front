/**
 * Created by ahadrii on 31/07/16.
 */

'use strict';

angular.module('main', ['ui.router', 'ngResource', 'angular-jwt', 'ui.bootstrap', 'toastr', 'ngFileUpload', 'btorfs.multiselect', 'angularjs-dropdown-multiselect', 'angularUtils.directives.dirPagination', 'angular-loading-bar']);

