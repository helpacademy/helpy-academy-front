/**
 * Created by ahadrii on 31/07/16.
 */

(function() {
'use strict';

angular
    .module('main')
    .config(config)

config.$inject = ['$stateProvider', '$urlRouterProvider', 'toastrConfig'];

function config($stateProvider, $urlRouterProvider, toastrConfig) {

  $urlRouterProvider.otherwise('/home'); // Set the default state's route

  $stateProvider
      .state('index', {
          abstract: true,
          url: '',
          views: {
              'header@': {
                  templateUrl: 'views/partials/header/header.html',
                  controller: 'headerCtr'
                }
            }
        })
      .state('index.home', {
          url: '/home',
          views: {
              'container@': {
                  templateUrl: 'views/partials/body/body.html'
                },
              'footer@': {
                  templateUrl: 'views/partials/footer/footer.html'
                  // controller: 'footerCtr'
              }
            }
        })
      .state('index.profile', {
          url: '/profile',
          views: {
              'container@': {
                  templateUrl: 'views/partials/profile/profile.html',
                  controller: 'profileCtr'
                }
            }
        })
      .state('index.dashboard', {
          url: '/dashboard',
          views: {
              'container@': {
                  templateUrl: 'views/partials/dashboard/dashboard.html',
                  controller: 'dashboardCtr',
                  controllerAs: 'dashboard'
                }
            }
        })
      .state('index.pending', {
          url: '/pending',
          views: {
              'container@': {
                  templateUrl: 'views/coming-soon/coming-soon.html',
                  controller: 'comingSoonCtr',
                  controllerAs: 'pending'
                }
            }
        });

    /**
     Toastr notification config
     **/
    angular.extend(toastrConfig, {
        allowHtml: true,
        closeButton: true,
        extendedTimeOut: 1000
    });
}



}());

