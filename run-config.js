/**
 * Created by ahadrii on 03/09/16.
 */



(function() {
    'use strict';

    angular
        .module('main')
        .config(configRun)

    configRun.$inject = ['$rootScope', 'cfp.loadingBar'];

    function configRun($rootScope, cfploadingBar) {

        $rootScope.$on('$routeChangeStart', function () {
            cfpLoadingBar.start();
            cfploadingBar.status() // Returns the loading bar's progress.
        });
        $rootScope.$on('$routeChangeSuccess', function () {
            cfploadingBar.status() // Returns the loading bar's progress.
            cfpLoadingBar.complete()
        });
        // Do the same with $routeChangeError
    }
});