/**
 * Created by ahadrii on 13/08/16.
 */

(function() {

'use strict';

angular
    .module('main')
    .controller('dashboardCtr', dashboardCtr);

dashboardCtr.$inject = [];

function dashboardCtr() {
  var vm = this;

  vm.main = true ;
  vm.mainDash = mainDash;
  vm.users = false ;
  vm.usersDash = usersDash;
  vm.courses = false ;
  vm.coursesDash = coursesDash;
  vm.charts = false ;
  vm.chartsDash = chartsDash;
    vm.activateMain = "active";
    vm.activateCharts = "";
    vm.activateUsers = "";
    vm.activateCourses = "";

  vm.modalAnimation = true ;
  // TODO : Minimize those functions
  function mainDash() {
    vm.charts = false ;
    vm.main = true ;
    vm.users = false ;
    vm.courses = false ;
      vm.activateMain = "active";
      vm.activateCharts = "";
      vm.activateUsers = "";
      vm.activateCourses = "";

  }
  function usersDash() {
    vm.charts = false ;
    vm.main = false ;
    vm.users = true ;
    vm.courses = false ;
      vm.activateMain = "";
      vm.activateCharts = "";
      vm.activateUsers = "active";
      vm.activateCourses = "";

  }
  function coursesDash() {
    vm.charts = false ;
    vm.main = false ;
    vm.users = false ;
    vm.courses = true ;
      vm.activateMain = "";
      vm.activateCharts = "";
      vm.activateUsers = "";
      vm.activateCourses = "active";

  }
  function chartsDash() {
    vm.charts = true ;
    vm.main = false ;
    vm.users = false ;
    vm.courses = false ;
      vm.activateMain = "";
      vm.activateCharts = "active";
      vm.activateUsers = "";
      vm.activateCourses = "";

  }

    vm.countProfessors = countProfessors;

    function countProfessors() {

    }

}

}());
