/**
 * Created by ahadrii on 29/08/16.
 */

(function() {

        'use strict';



        angular
            .module('main')
            .controller('yesnoModalCtr', yesnoModalCtr);

        yesnoModalCtr.$inject = ['$uibModalInstance', 'ProfessorService', 'toastr', 'professor'];

        function yesnoModalCtr($uibModalInstance, ProfessorService, toastr, professor) {

            var vm = this;

            vm.choice = choice
            function choice(result) {
                if(result) { // Change status of the professor
                    professor.status = !professor.status;
                    ProfessorService.update(professor, function(resp) {
                        if(resp.success) {
                        toastr.success('Status changé','Succes')
                            $uibModalInstance.close();
                        } else {
                        toastr.error(resp.msg,'Attention');
                        }
                    });
                }
                $uibModalInstance.close();
            }

        }
    }
)();
