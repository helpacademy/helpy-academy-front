/**
 * Created by ahadrii on 29/08/16.
 */

(function() {
        'use strict';
        angular
            .module('main')
            .controller('usersDashCtr', usersDashCtr);

         usersDashCtr.$inject = ['$rootScope', 'StudentService', 'ProfessorService', '$uibModal'];

        function usersDashCtr($rootScope, StudentService, ProfessorService, $uibModal) {

            var vm = this;

            /** Some IEFs to fetch data form the API **/
            (function getAllProfessors() {
                ProfessorService.get({}).$promise.then(function(response) {
                    if (response.success) {
                        $rootScope.professors = response.professors
                    } else {
                        return [];
                    }
                });
            })();

            (function getAllStudents() {
                StudentService.get({}).$promise.then(function(response) {
                    if (response.success) {
                        $rootScope.students = response.students
                    } else {
                        return [];
                    }
                });
            })();


            /** Professor logic**/
            vm.professorQuery = '';
            vm.professorsTab = false ;
            vm.showProfessors = showProfessors ;
            function showProfessors() {
                vm.studentsTab = false ;
                vm.professorsTab = true ;
            }

            vm.getStatus = getStatus ;
            function getStatus(professor) {
                if(professor.status) {
                    return 'Vérifieé';
                }
                return 'Non Vérifieé';
            }

            vm.changeStatus = changeStatus;

            function changeStatus(professor) {
                    vm.yesNoModal = $uibModal.open({
                    animation: vm.modalAnimation,
                    templateUrl: 'views/partials/dashboard/users/yesnoModal/yesnoModal.html',
                    controller: 'yesnoModalCtr',
                    controllerAs: 'yesno',
                    resolve: {
                        professor: professor
                    }
                });
            }

            vm.getProfSubjects = getProfSubjects ;

            function getProfSubjects(professor) {

                var subject = null ;
                var subjString = '';
                if(professor._subjects.length > 0) {
                for(subject of professor._subjects) {
                    subjString += subject.label + ', ';
                }
                    return subjString.substring(0 , subjString.length - 2) + '.';
                }
                return 'Pas de matières.';
            }


            /** Student logic**/
            vm.studentQuery = '';
            vm.studentsTab = true ;
            vm.showStudents = showStudents ;
            function showStudents() {
                vm.studentsTab = true ;
                vm.professorsTab = false ;
            }
            
            vm.showStudent = showStudent;
            function showStudent(student) {
                console.log(student);
            }
        }
    }
)();
