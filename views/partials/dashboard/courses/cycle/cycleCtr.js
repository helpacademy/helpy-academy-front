/**
     * Created by ahadrii on 13/08/16.
     */

(function() {

'use strict';



angular
    .module('main')
    .controller('cycleCtr', cycleCtr);

cycleCtr.$inject = ['$uibModalInstance', 'CycleService', 'toastr', '$rootScope'];

function cycleCtr($uibModalInstance, CycleService, toastr, $rootScope) {

    var vm = this;

    vm.reLoadCycles = reLoadCycles

    function reLoadCycles() {
        CycleService.get({}).$promise.then(function (response) {
            if (response.success) {
                var cycles = response.cycles;
                var cycle = {};
                var i = 0;
                for (cycle of cycles) { // This loop is just a temporary fixing for the multi-select angular module bug
                    cycle.id = i;
                    i++;
                }
                $rootScope.cycles = cycles;
            } else {
                return [];
            }
        });
    }

  vm.cycleLabel = null ;
  vm.save = save ;

  function save() {
    if (vm.cycleLabel) { // Adding the selected levels to the post request
        var cycle = {} ;
        cycle.label = vm.cycleLabel;
      CycleService.save(cycle, function(response) {
        if (response.success) {
          $uibModalInstance.close();
            toastr.success('Cycle crée','Succes');
            vm.reLoadCycles();
        } else {
            toastr.error(response.msg,'Erreur')
        }
      });
    } else {
        toastr.warning('S\'il vous plait entrer l\'intitulé du cycle','Attention')
    }
  }
    vm.close = close ;
    function close() {
        $uibModalInstance.close();
    }

}

}
    )();
