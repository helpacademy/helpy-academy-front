/**
 * Created by ahadrii on 13/08/16.
 */

(function() {

'use strict';

angular.module('main')
    .factory('CycleService', CycleService);

    CycleService.inject = ['$resource', '$window'];

function CycleService($resource, $window) {
  return $resource('http://localhost:3000/api/cycles', null, {
      save: {
          method: 'POST',
          headers: {'Authorization': getToken()}
        },
      get: {
          method: 'GET',
          headers: {'Authorization': getToken()}
      }
    });
  function getToken() {
    return $window.localStorage['jwtToken'];
  }
}

}());
