/**
     * Created by ahadrii on 13/08/16.
     */

(function() {

'use strict';

angular
    .module('main')
    .controller('subjectCtr', subjectCtr);

subjectCtr.$inject = ['$uibModalInstance', 'SubjectService', 'toastr', '$rootScope'];

function subjectCtr($uibModalInstance, SubjectService, toastr, $rootScope) {
    var vm = this;
    vm.levelSelected = [];
    vm.mutlilevelsfSett = {scrollable: true, selectionLimit: 1, enableSearch: true}; // Settings fo  the levels multi-select field!
    vm.profSelected = [];
    vm.mutliProfSett = {displayProp: 'lname', enableSearch: true}; // Settings fo  the professors multi-select field!
   
    vm.reLoadSubjects = reLoadSubjects;

    function reLoadSubjects() {
        SubjectService.get({}).$promise.then(function(response) {
            if (response.success) {
                $rootScope.subjects  = response.subjects; // Need to use it if not ==> funny bug!
            } else {
                return [];
            }
        });
    }
  


  vm.subjectLabel = null ;
  vm.close = close ;
  vm.save = save ;

  function close() {
    $uibModalInstance.close();
  }

  function save() {
    if (vm.subjectLabel && vm.levelSelected) {
      console.log(vm.subjectLabel);
        var subject = {};
        subject.label = vm.subjectLabel;
        subject._level = {};
        var level = null ;
        for(level of $rootScope.levels) {
            if(level.id === vm.levelSelected.id) {
                subject._level = level._id;
                break;
            }
        }
        subject._professors = [];
        if((vm.profSelected)) { // Adding the selected professors to the post request
            var professor = null;
            var profsSel = null ;
            for( profsSel of vm.profSelected) {
                for(professor of $rootScope.professors) {
                    if(professor.id === profsSel.id) {
                        subject._professors.push(professor._id)
                    }
                }
            }
        }
        SubjectService.save(subject, function(response) {
            if (response.success) {
                $uibModalInstance.close();
                toastr.success('Matière créee','Succes')
                vm.reLoadSubjects();
            } else {
                toastr.error(response.msg,'Erreur')
            }
        });
    } else {
        toastr.warning('S\'il vous plait remplir l\'intitulé du matière','Succes')
    }
  }



}

}
    )();
