/**
 * Created by ahadrii on 13/08/16.
 */

(function() {

'use strict';

angular.module('main')
    .factory('SubjectService', SubjectService);

SubjectService.inject = ['$resource', '$window'];

function SubjectService($resource, $window) {
  return $resource('http://localhost:3000/api/subjects', null, {
      save: {
          method: 'POST',
          headers: {'Authorization': getToken()}
        },
      get: {
          method: 'GET',
          headers: {'Authorization': getToken()}
        }
    });
  function getToken() {
    return $window.localStorage['jwtToken'];
  }
}

}());
