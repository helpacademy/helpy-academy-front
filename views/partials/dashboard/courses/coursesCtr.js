/**
 * Created by ahadrii on 14/08/16.
 */

(function() {

'use strict';
angular
    .module('main')
    .controller('coursesCtr', coursesCtr);

coursesCtr.$inject = ['$uibModal', 'SubjectService', 'LevelService', 'CycleService', 'ProfessorService', '$rootScope'];

function coursesCtr($uibModal, SubjectService, LevelService, CycleService, ProfessorService, $rootScope) {
      var vm = this;

    /** Some IEFs to load data form the API **/

    (function getAllLevels() {
        LevelService.get({}).$promise.then(function (response) {
            if (response.success) {
                var levels = response.levels;
                var level = {};
                var i = 0;
                for (level of levels) { // This loop is just a temporary fixing for the multi-select angular module bug
                    level.id = i;
                    i++;
                }
                $rootScope.levels = levels;
            } else {
                return [];
            }
        });
    })();
    
    (function getAllCycles() {
        CycleService.get({}).$promise.then(function (response) {
            if (response.success) {
                var cycles = response.cycles;
                var cycle = {};
                var i = 0;
                for (cycle of cycles) { // This loop is just a temporary fixing for the multi-select angular module bug
                    cycle.id = i;
                    i++;
                }
                $rootScope.cycles = cycles;
            } else {
                return [];
            }
        });
    })();

    (function getAllSubjects() {
        SubjectService.get({}).$promise.then(function(response) {
            if (response.success) {
                $rootScope.subjects  = response.subjects; // Need to use it if not ==> funny bug!
            } else {
                return [];
            }
        });
    })();

    (function getAllProfessors(){
        ProfessorService.get({}).$promise.then(function(res) {
            if (res.success) {
                var professors = res.professors;
                if (professors) {
                    var professor = null;
                    var i = 0;
                    for (professor of professors) { // This loop is just a temporary fixing for the multi-select angular module bug
                        professor.id = i;
                        i++;
                    }
                    vm.professors = professors;
                } else {
                    return [];
                }
            }
        })
    })();



    /** Subjects Table Stuff **/

    vm.selectSubj = selectSubj ;
    vm.showSubjects = showSubjects;
    vm.subjectsTab = true ;
    /** Search query **/
    vm.subjQuery = '';

    function selectSubj(subject) {
        console.log(subject);
    }

    function showSubjects() {
        vm.subjectsTab = true ;
        vm.specialsTab = false ;
        vm.levelsTab = false ;
        vm.cyclesTab = false ;
    }


    /** Level Table Stuff **/


    vm.selectLevel = selectLevel ;
    function selectLevel(level) {
        console.log(level);
    }
    vm.levelQuery = '';

    vm.showLevels = showLevels ;
    function showLevels() {
        vm.subjectsTab = false ;
        vm.specialsTab = false ;
        vm.levelsTab = true ;
        vm.cyclesTab = false ;
    }
    vm.levelsTab = false ;

















    /** Cycle Table Stuff **/
    vm.getLevelsOfCycle = getLevelsOfCycle;
    function getLevelsOfCycle(cycle) {
        if(cycle._levels) {
            var levelsString = '';
            var level = null ;
            for( level of cycle._levels ) {
                levelsString +=  level.label + ', ';
            }
            return levelsString == '' ? 'Pas de Niveaux.': levelsString.substring(0,  levelsString.length - 2) + '.';
        } else {
            return 'Pas de Niveaux.';
        }
    }
    vm.selectCycle = selectCycle ;
    function selectCycle(cycle) {
        console.log(cycle);
    }
    vm.cycleQuery = '';
    vm.selectCcl = selectCcl ;
    vm.showCycles = showCycles;
    vm.cyclesTab = false ;
    function selectCcl(cycle) {
        console.log(cycle);
    }
    function showCycles() {
        vm.subjectsTab = false ;
        vm.specialsTab = false ;
        vm.levelsTab = false ;
        vm.cyclesTab = true ;
    }




    /** CRUD of courses section **/
  vm.addSubject = addSubject;
   vm.addLevel = addLevel
    vm.addCycle = addCycle;



    function addSubject(size) {
    vm.addSubjectMdlInstance = $uibModal.open({
        animation: vm.modalAnimation,
        templateUrl: 'views/partials/dashboard/courses/subject/addmodal.html',
        controller: 'subjectCtr',
        controllerAs: 'subject',
        size: size,
        resolve: {}
      });
  }

    function addLevel(size) {
        vm.addLevelMdlInstance = $uibModal.open({
            animation: vm.modalAnimation,
            templateUrl: 'views/partials/dashboard/courses/level/addmodal.html',
            controller: 'levelCtr',
            controllerAs: 'level',
            size: size,
            resolve: {}
        });
    }

    function addCycle(size) {
        vm.addCycleMdlInstance = $uibModal.open({
            animation: vm.modalAnimation,
            templateUrl: 'views/partials/dashboard/courses/cycle/addmodal.html',
            controller: 'cycleCtr',
            controllerAs: 'cycle',
            size: size,
            resolve: {}
        });
    }

    /** Courses table section **/
    vm.subjects = true ;
 //   vm.specialities = true ;
    vm.levels = true ;
    vm.cycles = true ;


}
}
)();
