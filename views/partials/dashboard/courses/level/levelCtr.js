/**
     * Created by ahadrii on 13/08/16.
     */

(function() {

'use strict';



angular
    .module('main')
    .controller('levelCtr', levelCtr);

levelCtr.$inject = ['$uibModalInstance', 'LevelService', 'toastr', '$rootScope'];

function levelCtr($uibModalInstance, LevelService, toastr, $rootScope) {

    var vm = this;
    vm.reLoadLevels = reLoadLevels

    function reLoadLevels() {
        LevelService.get({}).$promise.then(function (response) {
            if (response.success) {
                var levels = response.levels;
                var level = {};
                var i = 0;
                for (level of levels) { // This loop is just a temporary fixing for the multi-select angular module bug
                    level.id = i;
                    i++;
                }
                $rootScope.levels = levels;
            } else {
                return [];
            }
        });
    }

    vm.cycleSelected = [];
    vm.mutliCyclesSett = {scrollable: true, selectionLimit: 1, enableSearch: true}; // Settings fo  the levels multi-select field!


    vm.levelLabel = null;
    vm.close = close;
    vm.save = save;


    function close() {
        $uibModalInstance.close();
    }

    function save() {
        if (vm.levelLabel && vm.cycleSelected) {
            var level = {};
            level.label = vm.levelLabel;
            if ((vm.cycleSelected)) {
                var cycle = null;
                for (cycle of $rootScope.cycles) {
                    if (cycle.id === vm.cycleSelected.id) {
                        console.log(cycle);
                        level._cycle = cycle._id;
                        LevelService.save(level, function (response) {
                            if (response.success) {
                                $uibModalInstance.close();
                                toastr.success('Niveau crée', 'Succes');
                                vm.reLoadLevels();
                                return;
                            } else {
                                toastr.error(response.msg, 'Erreur');
                                return;
                            }
                        });
                    }
                }
            }
            else {
                toastr.warning('S\'il vous plait entrer l\'intitulé du niveau et choisir un cycle', 'Attention')
            }
        }
    }
}
})();
