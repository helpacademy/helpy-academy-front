/**
 * Created by ahadrii on 13/08/16.
 */

(function() {

'use strict';

angular.module('main')
    .factory('LevelService', LevelService);

    LevelService.inject = ['$resource', '$window'];

function LevelService($resource, $window) {
  return $resource('http://localhost:3000/api/levels', null, {
      save: {
          method: 'POST',
          headers: {'Authorization': getToken()}
        },
      get: {
          method: 'GET',
          headers: {'Authorization': getToken()}
      }
    });
  function getToken() {
    return $window.localStorage['jwtToken'];
  }
}

}());
