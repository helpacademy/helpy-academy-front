/**
 * Created by ahadrii on 12/08/16.
 */

(function() {

'use strict';

angular.module('main')
    .factory('ProfessorService', ProfessorService);

    ProfessorService.inject = ['$resource', '$window'];

function ProfessorService($resource, $window) {
  return $resource('http://localhost:3000/api/professors', null, {
      update: {
          method: 'PUT',
          headers: {
              'Authorization': getToken()
            }
        },
      get: {
          method: 'GET',
          headers: {
              'Authorization': getToken()
          }
      }
    });
  function getToken() {
      console.log($window.localStorage['jwtToken']);
    return $window.localStorage['jwtToken'];
  }
}

}());
