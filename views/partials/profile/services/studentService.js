/**
 * Created by ahadrii on 26/08/16.
 */

(function() {

    'use strict';

    angular.module('main')
        .factory('StudentService', StudentService);

    StudentService.inject = ['$resource', '$window'];

    function StudentService($resource, $window) {
        return $resource('http://localhost:3000/api/students', null, {
            update: {
                method: 'PUT',
                headers: {
                    'Authorization': getToken()
                }
            },
            get: {
                method: 'GET',
                headers: {
                    'Authorization': getToken()
                }
            }
        });
        function getToken() {
            return $window.localStorage['jwtToken'];
        }
    }

}());
