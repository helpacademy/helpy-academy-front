/**
 * Created by ahadrii on 11/08/16.
 */

(function() {

'use strict';

angular
    .module('main')
    .controller('updateCtr', updateCtr);

updateCtr.$inject = ['$uibModalInstance','$window', 'StudentService', 'ProfessorService','toastr', '$state', '$rootScope', 'SubjectService'];

function updateCtr($uibModalInstance, $window, StudentService, ProfessorService, toastr, $state, $rootScope, SubjectService) {
  var vm = this ;

  vm.close = close;
  vm.user = getUser() ;
  vm.date_birth = new Date(vm.date_birth) ;
  vm.saveChanges = saveChanges;

  function saveUser(user) {
    $window.localStorage['user'] = JSON.stringify(user);
  }

  function saveToken(jwt) {
    $window.localStorage['jwtToken'] = jwt;
  }

  function getUser() {
    if ($window.localStorage['user']) {
      return JSON.parse($window.localStorage['user']);
    }
    return null ;
  }


    /* For updating professor's subjects **/
    vm.postSubjects = [];
    vm.subjectsSelected = [];
    vm.subjects = [];
    if(!$rootScope.isStudent()) {
        (function getAllSubjects() {
        SubjectService.get({}).$promise.then(function (response) {
            if (response.success) {
                var subjects = response.subjects;
                var subject = {};
                var i = 0;
                console.log(vm.user._subjects);
                for (subject of subjects) { // This loop is just a temporary fixing for the multi-select angular module bug
                    for(var subjId of vm.user._subjects) {
                        if(subjId === subject._id) {
                            vm.postSubjects.push({"id": i});
                            break;
                        }
                    }
                    subject.id = i;
                    i++;
                }
                vm.subjects = subjects;
            } else {
                return [];
            }
        });
    })();
        /** Updating professor subects logic **/
        vm.subjectsSelected = vm.postSubjects;
        vm.mutlilSubjSett = {enableSearch: true, scrollable: true}; // Settings fo  the professors multi-select field!

    }

console.log(vm.user);
  function saveChanges() {
    var updatedUser = {email: vm.user.email, fname: vm.user.fname, lname: vm.user.lname,
      phone: vm.user.phone, date_birth: vm.date_birth.toLocaleDateString(), _id: vm.user._id, desc: vm.user.desc};
    if(!$rootScope.isStudent()) // it's a professor Or an Admin
    {
        updatedUser.profession = vm.user.profession;
        updatedUser._subjects = [];
        if(vm.subjectsSelected) { // Adding the selected subjects to the post request
            var subject = null;
            var subjSel = null ;
            for( subjSel of vm.subjectsSelected) {
                for(subject of vm.subjects) {
                    if(subject.id === subjSel.id) {
                        updatedUser._subjects.push(subject._id)
                    }
                }
            }
            vm.postSubjects = [];
        }

        ProfessorService.update(updatedUser, function(resp) {
        saveUser(resp.user);
        saveToken(resp.token);
          $state.reload();
          $uibModalInstance.close();
          toastr.success('Votre compte est met à jour','Succes');
      });
    } else { // it's a student
        StudentService.update(updatedUser, function(resp) {
        saveUser(resp.user);
        saveToken(resp.token);
         $state.reload();
          $uibModalInstance.close();
          toastr.success('Votre compte est met a jour','Succes');
      });
    }
  }
  function close() {
    $uibModalInstance.close();
  }

/** Date picker logic**/
    vm.openedDP = false;
    vm.datePicker = datePicker ;
    function datePicker() {
        vm.openedDP = !vm.openedDP;
        console.log(vm.date_birth.toLocaleDateString());
    };
    vm.altInputFormats = ['M!-d!-yyyy'];








}

}());
