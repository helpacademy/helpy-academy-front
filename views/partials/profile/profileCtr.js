/**
 * Created by ahadrii on 11/08/16.
 */

(function() {

'use strict';

angular
    .module('main')
    .controller('profileCtr', headerCtr);

headerCtr.$inject = ['$uibModal', '$window', 'Upload', '$scope', '$timeout', '$rootScope'];

function headerCtr($uibModal, $window, Upload, $scope, $timeout, $rootScope) {
  var vm = this;

  vm.open = open;
  vm.modalAnimation = true;
  vm.user = getUser() ;
  vm.user_name = user_name;


    function getUser() {
        if ($window.localStorage['user']) {
            return JSON.parse($window.localStorage['user']);
        }
        return null ;
    }
    vm.bio = bio;
    vm.showBio = true;
    function bio() {
        if($rootScope.isStudent()) {
            if(vm.user.desc) {
                return vm.user.desc;
            } else {
                vm.showBio = false ;
                return '';
            }
        }  else {
            if(vm.user.profession) {
                return vm.user.profession;
            } else {
                vm.showBio = false ;
                return '';
            }
        }


    }
    vm.phone = getPhone;
    function getPhone() {
        if(vm.user.phone) {
            return vm.user.phone;
        } else {
            return 'Non disponible';
        }
    }
  vm.date_birth = getDateBirth;

      function getDateBirth() {
        if(vm.user.date_birth) {
            return vm.user.date_birth;
        } else {
            return 'Non disponible';
        }
      }
  vm.uploadPhoto = uploadPhoto ;
  vm.getImage = getImage ;

    console.log(getUser());

    $scope.url = 'http://localhost:3000/' + getUser()._id + '.jpg';
  function getImage() {
    return 'http://localhost:3000/' + getUser()._id + '.jpg';
  }

  function user_name() {
    return getUser().fname + ' ' + getUser().lname;
  }



  function open(size) {
    vm.updateModalInstance = $uibModal.open({
      animation: vm.modalAnimation,
      templateUrl: 'views/partials/profile/updateProfile/updateMdl.html',
      controller: 'updateCtr',
      controllerAs: 'update',
      size: size,
      resolve: {
      }
    });
  }

  function uploadPhoto(file) {
    if (file) {
      Upload.rename(file, getUser()._id.toString() + '.jpg'); // Rename the image with the user _id
      Upload.upload({
          url: 'http://localhost:3000/api/profilephoto',
          data: {'profilePic': file},
          method: 'POST'
        }).then(function(resp) {
          var random = (new Date()).toString();
          $scope.url = 'http://localhost:3000/' + getUser()._id + '.jpg' + '?' + random;
          $timeout(function(){ $scope.$apply()}); // Waiting until the next $digest
      });
    }
  }

    $rootScope.isStudent = isStudent;
    function isStudent() {
        if(vm.user.isAdmin === undefined) // it's a professor Or an Admin
        {
            return true ;
        } else { // it's a student
            return false ;
        }
    }


    /** Rating logic **/
    vm.rate = vm.user.rate;
    vm.max = 5;
    vm.isReadOnly = true;
    vm.hoveringOver = function(value) {
        vm.overStar = value;
        vm.percent = 100 * (value / vm.max);
    };

    vm.getAvis = getAvis;

    function getAvis() {
        if(vm.user.avis) {
            return vm.user.avis + ' Avis'
        }
        return '0 Avis';
    }

    vm.getStatus = getStatus ;

    function getStatus() {
        return vm.user.status ? 'Compte Vérifieé' : 'Compte Non Vérifieé';
    }













}
}());
