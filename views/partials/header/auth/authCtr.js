/**
 * Created by ahadrii on 01/08/16.
 */

(function() {

'use strict';

angular
    .module('main')
    .controller('authModalCtr', authModalCtr);

authModalCtr.$inject = ['LoginService', '$window', 'ProfessorService', 'StudentService', '$uibModalInstance', '$state', 'toastr'];

function authModalCtr(LoginService, $window, ProfessorService, StudentService, $uibModalInstance, $state, toastr) {
  var vm = this ;

  vm.close = close;
  vm.init = init ;
  vm.saveUser = saveUser;
  vm.password = null;
  vm.email = null;
  vm.fname = null;
  vm.lname = null;
  vm.phone = null;
  vm.user = null ;
  // Login vm
  vm.rememberMe = false;
  vm.jwt = null;
  vm.password2 = null;
  vm.signIn = signIn ;
  vm.saveToken = saveToken;
  // SignUp vm
  vm.signUp = signUp;
  vm.flag = false ;
  vm.isPending = isPending;

  function isPending() {
    var currentState = $state.current.name;
    if (currentState === 'index.pending') {
      return true ;
    }
    return false ;
  }
    
  function signIn() {
    if (vm.email && vm.password) {
        console.log(vm.email, vm.password);
      LoginService.loginUser(vm.email, vm.password).then(
          function(loginResult) {
            console.log(loginResult);
            if (loginResult.success) {
              vm.saveUser(loginResult.user);
              vm.saveToken(loginResult.token); // Save the user and it's jwt in the cookies => Local Storage
              if (!vm.flag) {
                  toastr.success('Bienvenue ' + JSON.parse($window.localStorage['user']).fname + ' ' + JSON.parse($window.localStorage['user']).lname, 'Connexion réussie');
              }
              vm.flag = false ;
              vm.close();
            } else {
                toastr.error(loginResult.msg, 'Error');
            }
          },
          function(err) {
              toastr.error('Le site est en maintenance maintenant, Svp Réessayez plus tard', 'Erreur');
          });
    } else {
        toastr.error('S\'il vous plaît remplir toutes les informations.', 'Erreur');
    }
  }

  function saveUser(user) {
    $window.localStorage['user'] = JSON.stringify(user);
  }

  function saveToken(jwt) {
    $window.localStorage['jwtToken'] = jwt;
  }

  function signUp() {
      if (vm.password === vm.password2) {
      if (vm.email && vm.password && vm.fname && vm.lname && vm.user) {
        // SignUp Logic
        var newUser = {'email': vm.email, 'password': vm.password, 'fname': vm.fname,
          'lname': vm.lname};
          console.log(newUser);
          if(vm.user === 'professor') {
              ProfessorService.save(newUser, function(resp) {
                  if (resp.success) {
                      var currentState = $state.current.name;
                      if (currentState === 'index.pending') {
                          toastr.success(vm.fname + ' ' + vm.lname + '<em> Merci pour votre inscription, soyez prêt l\'ouverture est prôche.</em>', 'Compte crée');
                          $uibModalInstance.close();
                          return;
                      }
                      signIn();
                  } else {
                      console.log(resp.msg);
                      toastr.error(resp.msg, 'Erreur');
                  }
              }, function(err) {
                  toastr.error(err.msg, 'Erreur');
              });
          } else {
              StudentService.save(newUser, function(resp) {
                  if (resp.success) {
                      var currentState = $state.current.name;
                      if (currentState === 'index.pending') {
                          toastr.success(vm.fname + ' ' + vm.lname + '<em> Merci pour votre inscription, soyez prêt l\'ouverture est prôche.</em>', 'Compte crée');
                          $uibModalInstance.close();
                          return;
                      }
                      signIn();
                  } else {
                      toastr.error(resp.msg, 'Erreur');
                  }
              }, function(err) {
                  toastr.error(err.msg, 'Erreur');
              });
          }

      } else {
          toastr.warning('S\'il vous plaît remplir toutes les informations.', 'Attention');
      }
    } else {
          toastr.warning('Les mot de passes donnés ne correspondent pas', 'Attention');
      }
  }

  function init() {
    vm.password = null;
    vm.email = null;
    vm.fname = null;
    vm.lname = null;
    vm.phone = null;
  }

  function close() {
    $uibModalInstance.close();
  }

}

}());
