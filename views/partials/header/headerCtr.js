/**
 * Created by ahadrii on 01/08/16.
 */

(function() {

'use strict';

angular
    .module('main')
    .controller('headerCtr', headerCtr);

headerCtr.$inject = ['$uibModal', '$window', 'jwtHelper', '$state', 'toastr'];

function headerCtr($uibModal, $window, jwtHelper, $state, toastr) {
  var vm = this;

  vm.open = open;
  vm.modalAnimation = true;
  vm.getUser = getUser;
  vm.getToken = getToken;
  vm.isAuthed = isAuthed;
  vm.isAdmin = isAdmin;
  vm.fullName = fullName;
  vm.logOut = logOut ;

    function isAdmin() {
        if(vm.getUser()) {
            if(vm.getUser().isAdmin) {
                return true;
            }
            return false ;
        } else {
            return false;
        }
    }

  function logOut() {

    toastr.success('À la prochaine :) ','Succes')
    vm.user = null ;
    vm.isLogged = false;
    $window.localStorage.removeItem('jwtToken');
    $window.localStorage.removeItem('user');
    $state.go('index.home');

  }

  function getToken() {
    return $window.localStorage['jwtToken'];
  }

  function fullName() {
    if (vm.getUser()) {
      return vm.getUser().fname + ' ' + vm.getUser().lname;
    }
  }

  function isAuthed() {
    var token = vm.getToken();
    if (token) {
      token = token.split(' ')[1];
      var isExpired = jwtHelper.isTokenExpired(token);
      return !isExpired;
    }
  }

  function getUser() {
    if ($window.localStorage['user']) {
      return JSON.parse($window.localStorage['user']);
    }
  }

  function open(size) {
    vm.authModalInstance = $uibModal.open({
      animation: vm.modalAnimation,
      templateUrl: 'views/partials/header/auth.html',
      controller: 'authModalCtr',
      controllerAs: 'authCtr',
      size: size,
      resolve: {
      }
    });

  }

}
}());
