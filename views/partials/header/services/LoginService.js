/**
 * Created by ahadrii on 02/08/16.
 */

(function() {

'use strict';

angular.module('main')
    .factory('LoginService', ['$resource', function($resource) {
        var LoginResource = $resource('http://localhost:3000/api/auth/login');
        var serviceObject = {loginUser: function(email, password) {
            return LoginResource.save({}, {email: email, password: password}).$promise;
          }};
        return serviceObject;
      }]);
}());
