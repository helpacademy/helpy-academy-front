/**
 * Created by ahadrii on 31/08/16.
 */


(function() {

    'use strict';

    angular.module('main')
        .factory('SearchService', SearchService);

    SearchService.inject = ['$resource'];

    function SearchService($resource) {
        return $resource('http://localhost:3000/api/search/:keyword', null, {
            get: {
                method: 'GET'
            },
            params: {
                keyword: '@keyword'
            }
        });
    }

}());
