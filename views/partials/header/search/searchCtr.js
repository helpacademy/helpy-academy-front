/**
 * Created by ahadrii on 31/08/16.
 */

/**
 * Created by ahadrii on 01/08/16.
 */

(function() {

    'use strict';

    angular
        .module('main')
        .controller('searchCtr', searchCtr);

    searchCtr.$inject = ['SearchService'];

    function searchCtr(SearchService) {
        var vm = this;

        vm.query = "";
        vm.search = search;
        vm.results = []

        function search() {
            if(vm.query === "") {
                vm.results = []
            } else {
            SearchService.get({keyword: vm.query}, function(response)
            {
                if(response.success) {
                    vm.results = response.result;
                } else {
                    console.log(response.msg);
                }
            });
            }
        }


    }
}());
