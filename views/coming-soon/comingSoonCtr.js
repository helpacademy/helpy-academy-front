/**
 * Created by ahadrii on 23/08/16.
 */

(function() {

'use strict';

angular
    .module('main')
    .controller('comingSoonCtr', comingSoonCtr);

comingSoonCtr.$inject = ['$uibModal'];

function comingSoonCtr($uibModal) {


  var vm = this;

  vm.open = open ;

  function open(size) {
    vm.authModalInstance = $uibModal.open({
        animation: vm.modalAnimation,
        templateUrl: 'views/partials/header/auth.html',
        controller: 'authModalCtr',
        controllerAs: 'authCtr',
        size: size,
        resolve: {}
      });

  }
}
}());
