/**
 * Created by ahadrii on 29/07/16.
 */

var gulp = require('gulp');
var args = require('yargs').argv;
var config = require('./gulp.config.js')();
var browserSync = require('browser-sync');

var $ = require('gulp-load-plugins')({
    lazy: true
  });

var port = process.env.PORT || config.defaultPort ;

gulp.task('lint', function() {
    log('Analyzing source with JSHint and JSCS');

    return gulp
        .src(config.alljs)
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', {
            verbose: true}))
        .pipe($.jshint.reporter('fail'));

  });

gulp.task('inject', function() {
    log('Injecting css js into our html');

    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.js, {read: false}), {ignorePath: 'src/client'}))
        .pipe($.inject(gulp.src(config.css, {read: false}), {ignorePath: 'src/client'}))
        .pipe(gulp.dest(config.views));
  });

gulp.task('serve-dev', function() {
    var isDev = true;

    var nodeOptions = {
        script: config.nodeServer,
        delayTime: 1,
        env: {
            'PORT': port,
            'NODE_ENV': isDev ? 'dev' : 'build'
          },
        watch: [config.server]
      };

    return $.nodemon(nodeOptions)
        .on('restart', function(ev) {
            log('** nodemon restarted **');
            log('** files changed on restart \n:' + ev);
            setTimeout(function() {
                browserSync.notify('reloading now ...');
                browserSync.reload({
                    stream: false
                  });
              }, config.browserReloadDelay);
          })
        .on('start', function() {
            log('** nodemon started **');
            startBrowserSync();
          })
        .on('crash', function() {
            log('** nodemon crashed: script crashed for some reason **');
          })
        .on('exit', function() {
          log('** nodemon exited cleanly **');
        });
  });

function log(msg) {
  if (typeof (msg) === 'object') {
    for (var item in msg) {
      if (msg.hasOwnProperty(item)) {
        $.util.log($.util.colors.blue(msg[item]));
      }
    }
  } else {
    $.util.log($.util.colors.blue(msg));
  }
}

function  startBrowserSync() {
  if (args.nosync || browserSync.active) {
    return;
  }

  log('Starting browser-sync on port ' + port);

  var options = {
      proxy: 'localhost:' + port,
      port: 3000,
      files: [config.client  , config.server],
      ghostMode: {
          clicks: true,
          location: false,
          forms: true,
          scroll: true
        },
      injectChanges: true,
      logFileChanges: true,
      logLevel: 'debug',
      logPrefix: 'browSync',
      notify: true,
      reloadDelay: 0
    };

  browserSync(options);

}
